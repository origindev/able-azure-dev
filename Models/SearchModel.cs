﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace AbleSystems.Web.Models
{
    public class SearchModel
    {
        public List<IPublishedContent> results { get; set; }

        public string text { get; set; }

        public int totalResults { get; set; }

        public int resultsPerPage { get; set; }

        public int totalPages { get; set; }

        public SearchModel()
        {
            this.results = new List<IPublishedContent>();
            resultsPerPage = 6;
        }
    }
}