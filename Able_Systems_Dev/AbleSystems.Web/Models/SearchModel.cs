﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace AbleSystems.Web.Models
{
    public class SearchModel
    {
        public List<IPublishedContent> results { get; set; }

        public string Text { get; set; }

        public int TotalResults { get; set; }

        public int ResultsPerPage { get; set; }

        public int TotalPages { get; set; }

        public SearchModel()
        {
            this.results = new List<IPublishedContent>();
            ResultsPerPage = 6;
        }
    }
}