﻿using System.ComponentModel.DataAnnotations;

namespace AbleSystems.Web.Models
{
    public class LoginRegisterModel
    {
        /*  Register properties */

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address (your username)")]
        public string Email { get; set; }

       // [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

       // [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

      //  [Required]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

      /*  [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
       * */

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        /*  Login properties    */

        [Display(Name = "Password")]
        public string LoginPass { get; set; }

        public bool productUpdates { get; set; }


    }
}