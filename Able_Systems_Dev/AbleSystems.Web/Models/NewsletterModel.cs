﻿using System.ComponentModel.DataAnnotations;

namespace AbleSystems.Web.Models
{
    public class NewsletterModel
    {
        /*  Newsletter Registration properties */

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
    }
}