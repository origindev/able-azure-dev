﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbleSystems.Web.Controllers;

namespace AbleSystems.Web.Models
{
    public class ContactFormModel
    {
        public int ThankYouPageId { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        [DataType(DataType.Text)]
        [Display(Name = "Your First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your last name")]
        [DataType(DataType.Text)]
        [Display(Name = "Your Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [DataType(DataType.Text)]
        [EmailAddress]
        [Display(Name = "Your Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your company")]
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        [DataType(DataType.Text)]
        [Display(Name = "Phone number")]
        public string Phone { get; set; }

        public List<SelectListItem> SolutionItems; // list of selectable solutions for dropdown

        [Required(ErrorMessage = "Please select a solution")]
        [DataType(DataType.Text)]
        [Display(Name = "solution")]
        public string Solution { get; set; }

        //public List<SelectListItem> OtherItems; // list of selectable other items (as suggested in design - unclear what used for in practice)

        //[Required(ErrorMessage = "Please select another option")]
        //[DataType(DataType.Text)]
        //[Display(Name = "other")]
        //public string Other { get; set; }

        [Display(Name = "Tick here if you do not wish to be added to our mailing list")]
        public bool MailingList { get; set; }

        [Required(ErrorMessage = "Please enter a message")]
        [DataType(DataType.Text)]
        [Display(Name = "Your message")]
        public string Message { get; set; }

        public ContactFormModel(string item)
        {
            SolutionItems = new List<SelectListItem>();

            if (string.IsNullOrEmpty(item))
            {
                SolutionItems.Add(new SelectListItem { Text = "Place an order", Value = "Place an order", Selected = false });
                SolutionItems.Add(new SelectListItem { Text = "Request a quotation", Value = "Request a quotation", Selected = false });
                SolutionItems.Add(new SelectListItem { Text = "Request a call back", Value = "Request a call back", Selected = false });
                SolutionItems.Add(new SelectListItem { Text = "Technical support", Value = "Technical support", Selected = false });
                SolutionItems.Add(new SelectListItem { Text = "General Enquiry", Value = "General Enquiry", Selected = true });
                return;
            }

            var homes = SiteHelper.GetSiteRoots();
            if (homes != null)
            {
                var homepage = homes.FirstOrDefault();
                if (homepage != null)
                {
                    var solutions = homepage.Children.Where(x => x.DocumentTypeAlias.Equals("solutionType"));

                    // not clear what this used for, but if solutions is the industry / product solutions
                    // from website, we might populate this automatically from Umbraco

                    if (solutions.Any())
                    {
                        foreach (var solutionsParentNode in solutions)
                        {
                            if (solutionsParentNode != null)
                            {
                                foreach (var solution in solutionsParentNode.Children)
                                {
                                    SolutionItems.Add(new SelectListItem {Text = solution.Name, Value = solution.Id.ToString(), Selected = (solution.Name.ToLower() == item.ToLower())});

                                    if (solutionsParentNode.Name.Contains("Our Product Range"))
                                    {
                                        foreach (var solutionChild in solution.Children)
                                        {
                                            if (!SolutionItems.Any(x => x.Text == solutionChild.Name))
                                            {
                                                SolutionItems.Add(new SelectListItem { Text = solutionChild.Name, Value = solutionChild.Id.ToString(), Selected = (solutionChild.Name.ToLower() == item.ToLower())});
                                            }
                                            SolutionItems.Add(new SelectListItem { Text = solutionChild.Name + " free sample kit", Value = solutionChild.Id.ToString(), Selected = (solutionChild.Name.ToLower() + " free sample kit" == item.ToLower()) });                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }           
        }

        public ContactFormModel()
        {           
        }
    
    }
}