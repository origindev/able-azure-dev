﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AbleSystems.Web.Controllers
{
    public static class SiteCacheHelper
    {
        public static void AddToCache(string key, object value, TimeSpan timespan)
        {
            HttpRuntime.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, timespan, CacheItemPriority.Normal,
                null);
        }

        public static void AddToRequestCache(string key, object value)
        {
            HttpContext.Current.Items.Add(key, value);
        }

        public static object GetFromCache(string key)
        {
            return HttpRuntime.Cache[key];
        }

        public static object GetFromRequestCache(string key)
        {
            return HttpContext.Current.Items[key];
        }

        public static void InvalidateCacheObject(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        public static void ExpireAllCacheItems()
        {
            var enumerator = HttpRuntime.Cache.GetEnumerator();
            var cacheItems = new Dictionary<string, object>();

            while (enumerator.MoveNext())
                cacheItems.Add(enumerator.Key.ToString(), enumerator.Value);

            foreach (var key in cacheItems.Keys)
                HttpRuntime.Cache.Remove(key);
        }

    }

    public static class SiteHelper
    {
        private static UmbracoBuilder _umbracoBuilder = new UmbracoBuilder();
        public static UmbracoHelper GetUmbracoHelper()
        {
            var umbracoHelper = SiteCacheHelper.GetFromCache("umbracoHelper") as UmbracoHelper;

            if (umbracoHelper != null) return umbracoHelper;
            umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            SiteCacheHelper.AddToCache("umbracoHelper", umbracoHelper, new TimeSpan(1, 0, 0));

            return umbracoHelper;
        }


        private static IEnumerable<IPublishedContent> GetRootNodes()
        {
            var siteRootNode = SiteCacheHelper.GetFromCache("SiteRootNode") as IEnumerable<IPublishedContent>;

            if (siteRootNode != null) return siteRootNode;
            siteRootNode = GetUmbracoHelper().TypedContentAtRoot();
            SiteCacheHelper.AddToCache("SiteRootNode", siteRootNode, new TimeSpan(1, 0, 0));

            return siteRootNode;
        }

        public static IPublishedContent GetGlobalSettingsNode()
        {
            var globalSettingsNode = SiteCacheHelper.GetFromCache("GlobalSettingsNode") as IPublishedContent;

            if (globalSettingsNode != null) return globalSettingsNode;
            globalSettingsNode = GetSiteSettingsNode().Children.First(x => x.IsDocumentType("GlobalSettings"));
            SiteCacheHelper.AddToCache("GlobalSettingsNode", globalSettingsNode, new TimeSpan(1, 0, 0));

            return globalSettingsNode;
        }

        private static IPublishedContent GetSiteSettingsNode()
        {
            var siteSettingsNode = SiteCacheHelper.GetFromCache("SiteSettingsNode") as IPublishedContent;

            if (siteSettingsNode != null) return siteSettingsNode;
            siteSettingsNode = GetUmbracoHelper().TypedContentAtRoot().First(x => x.IsDocumentType("SiteSettings"));
            SiteCacheHelper.AddToCache("SiteSettingsNode", siteSettingsNode, new TimeSpan(1, 0, 0));

            return siteSettingsNode;
        }

        /// <summary>
        /// Helper method to get data from Global Settings - checks if it exists
        /// returns a value if so or an empty string if not
        /// Also caches the result so avoid db round trips
        /// </summary>
        /// <param name="propertyName">Property alias to check</param>
        /// <returns>String value if valid or empty string if not</returns>
        public static string GetSiteGlobalProperty(string propertyName)
        {
            if (GetGlobalSettingsNode().HasProperty(propertyName) && GetGlobalSettingsNode().HasValue(propertyName))
            {
                return GetGlobalSettingsNode().GetPropertyValue<string>(propertyName);
            }
            return "";
        }

        /// <summary>
        /// Helper method to get root node(s) of site
        /// returns a a list of nodes that are found in the root of Umbraco
        /// </summary>
        /// <returns>a list of (PublishedContent) nodes found in the root of Umbraco</returns>
        public static IEnumerable<IPublishedContent> GetSiteRoots()
        {
            return GetRootNodes();
        }

        /// <summary>
        /// Helper method to get the main Site Settings Node
        /// returns a value if so or an empty string if not
        /// </summary>
        /// <returns>(IPublishedContent) node that is the Site Settings node</returns>
        public static IPublishedContent GetSiteSettings()
        {
            return GetSiteSettingsNode();
        }


        public static string ThemeSelector(IPublishedContent node)
        {
            return node.GetPropertyValue<bool>("selectLightTheme") ? "light-theme" : "dark-theme";
        }


        /// <summary>
        /// Helper method to get an image from propertyAlias of node, getting 'cropAlias' if set,
        /// Otherwise returns full image Url (or if cannot get crop for any reason)
        /// returns 
        /// </summary>
        /// <returns>(string) Url for that imagenode </returns>
        public static string GetImageUrl(IPublishedContent node, string propertyAlias, string cropAlias = null)
        {
            var imageUrl = string.Empty;

            if (node.HasProperty(propertyAlias) && node.HasValue(propertyAlias))
            {
                var imageNode = GetUmbracoHelper().TypedMedia(node.GetPropertyValue(propertyAlias));
                if (imageNode != null)
                {
                    if (cropAlias != null)
                    {
                        var croppedUrl = imageNode.GetCropUrl(cropAlias);
                        if (!string.IsNullOrEmpty(croppedUrl))
                            imageUrl = croppedUrl;

                    }
                    else
                        imageUrl = imageNode.Url;
                }
            }

            return imageUrl;
        }

        /// <summary>
        /// Helper method that will (for any given IPublishedContent 'node') test 'componentPicker'
        /// property and will return the HTML String result of building all components picked 
        /// </summary>
        /// <returns>(HtmlString) of the HTML result of rendering all components on 'componentsPicker' property for 'node'</returns>
        public static HtmlString RenderPageComponents(IPublishedContent node)
        {
            var sb = new StringBuilder();

            if (node.HasValue("componentPicker"))
            {
                var bannerListValue = node.GetPropertyValue<string>("componentPicker");
                var bannerList =
                    bannerListValue.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                var bannerCollection = GetUmbracoHelper().TypedContent(bannerList).Where(x => x != null);
                foreach (var item in bannerCollection)
                {
                    if (item.DocumentTypeAlias.Equals("productRangeComponent"))
                        sb.Append(GetProductRangeHtml(node, item));
                    
                    else if (item.DocumentTypeAlias.Equals("productRangeTabs"))
                        sb.Append(GetProductRangeTabsHtml(node, item));

                    else
                        sb.Append(MvcHtmlString.Create(umbraco.library.RenderTemplate(item.Id)));
                    
                }
            }

            return new HtmlString(sb.ToString());
        }

        public static string GetOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }

        }
        private static HtmlString GetProductRangeHtml(IPublishedContent node, IPublishedContent item)
        {
            var umbracoHelper = SiteHelper.GetUmbracoHelper();
            var sb = new StringBuilder();
            var tabName = item.UrlName;

            sb.Append("<div class=\"product-range " + ThemeSelector(item) + "\"id=\"" + tabName + "\">");
            sb.Append("<div class=\"container\"><div class=\"row\"><div class=\"col-xs-12\">");

            if (item.HasProperty("articleTitle") && item.HasValue("articleTitle"))
            {
                sb.Append("<header class=\"wrapper-header\"><h2>" + item.GetPropertyValue<string>("articleTitle") +
                          "</h2>");
                sb.Append(item.GetPropertyValue<string>("bodyText"));
                sb.Append("</header>");
            }

            sb.Append("</div></div>");

            sb.Append("<div class=\"row product-range-carousel\">\n");

            var productRange = new List<string>();

            if (item.HasProperty("productRange") && item.HasValue("productRange"))
            {
                productRange = item.GetPropertyValue<string>("productRange").Split(',').ToList();
            }

            foreach (var productId in productRange)
            {
                var product = umbracoHelper.TypedContent(productId);

                if (product != null)
                {
                    var productName = product.Name;
                    var productImage = string.Empty;
                    var productLink = "#";
                    var productLinkText = "View";

                    if (product.HasValue("productTitle"))
                    {
                        productName = product.GetPropertyValue<string>("productTitle");
                    }

                    if (product.HasValue("productImage"))
                    {
                        var productImageNode =
                            umbracoHelper.TypedMedia(product.GetPropertyValue<string>("productImage"));
                        if (productImageNode != null)
                        {
                            productImage = productImageNode.Url;
                        }
                    }

                    if (product.HasValue("productLink"))
                    {
                        var productLinkNode =
                            umbracoHelper.TypedContent(product.GetPropertyValue<string>("productLink"));
                        if (productLinkNode != null)
                        {
                            productLink = productLinkNode.Url;
                        }
                    }

                    if (product.HasValue("productLinkText"))
                    {
                        var productLinkNode =
                            umbracoHelper.TypedContent(product.GetPropertyValue<string>("productLinkText"));

                        productLinkText = product.GetPropertyValue<string>("productLinkText");

                    }

                    sb.Append("<div class=\"col-xs-12 col-sm-6 col-md-4 pod-wrap\">\n");
                    sb.Append("<div class=\"pod\">\n");
                    sb.Append("<div class=\"img\" style=\"background-image: url(" + productImage + ")\">\n");
                    sb.Append("<a href=\"" + productLink + "\"></a></div><header><h3>" + productName + "</h3></header>\n");
                    sb.Append(product.GetPropertyValue("productDescription"));
                    sb.Append("<a href=\"" + product.Url + "\" class=\"custom-btn outlined red large pill\">" +
                              productLinkText + "</a>\n");
                    sb.Append("</div></div>\n");

                }
            }

            sb.Append("</div></div></div>");

            return new HtmlString(sb.ToString());
        }


        private static HtmlString GetProductRangeTabsHtml(IPublishedContent parentNode, IPublishedContent tabComponent)
        {
            var sb = new StringBuilder();
            var umbracoHelper = GetUmbracoHelper();

            if (parentNode.HasProperty("componentPicker") && parentNode.HasValue("componentPicker"))
            {
                var bannerListValue = parentNode.GetPropertyValue<string>("componentPicker");
                var bannerList =
                    bannerListValue.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                var bannerCollection = GetUmbracoHelper().TypedContent(bannerList).Where(x => x != null);
                var itemsPerPage = 8;


                var banners = bannerCollection.Where(x => x.DocumentTypeAlias.Equals("productRangeComponent"));

                sb.Append(string.Format("<ul class=\"internal-navigation list-unstyled item-number-{0}\">", banners.Count()));

                foreach (var banner in banners)
                {
                    var linkId = "#" + banner.UrlName;
                    var bannerName = banner.Name;
                    if (banner.HasProperty("articleTitle") && banner.HasValue("articleTitle"))
                    {
                        bannerName =  banner.GetPropertyValue<string>("articleTitle");
                    }
                    
                    sb.Append(string.Format(
                              "<li><a href=\"{0}\">{1}<i class=\"fa fa-chevron-circle-down\" aria-hidden=\"true\"></i></a>",
                              linkId, bannerName));
                }
                
                    sb.Append("</ul>");
            }

            //sb.Append(string.Format("<ul class=\"internal-navigation list-unstyled item-number-{0}\">", ));
       
            return new HtmlString(sb.ToString());
        }

    }

}
