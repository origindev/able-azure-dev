﻿#region info

// 
//  Copyright (C) 2016 Simon Antony Ltd www.simonantony.net - All Rights Reserved
//  You may use, distribute and modify this code under the
//  terms of the supplied license to the company that commissioned
//  this work, any other copying or usage by unauthorised 3rd parties is forbidden
// 
// Written by: Simon Steed
// Client: AbleSystems
// Project Name: AbleSystems.Web
// Date: 29/09/2016

#endregion

#region Using Clauses

using System;
using Umbraco.Core.Models;
using Umbraco.Web;

#endregion

namespace AbleSystems.Web.Controllers
{
    public class UmbracoBuilder
    {
        private readonly UmbracoHelper _publishedContent = new UmbracoHelper(UmbracoContext.Current);

        public string GetStringValue(IPublishedContent node, string aliasName)
        {
            var cacheKey = node.Id + aliasName;
            var dataKey = SiteCacheHelper.GetFromCache(cacheKey)as string;
            if (!string.IsNullOrEmpty(dataKey))
            {
                return dataKey;
            }

            var resultString = "";
            if (node.GetPropertyValue(aliasName) != null)
            {
                resultString = node.GetPropertyValue(aliasName).ToString();
            }

            SiteCacheHelper.AddToCache(cacheKey, resultString, new TimeSpan(1, 0, 0));

            return resultString;
        }

        public string GetImageMediaId(IPublishedContent node, string aliasName)
        {
            var cacheKey = node.Id + aliasName;
            var dataKey = SiteCacheHelper.GetFromCache(cacheKey) as string;
            if (!string.IsNullOrEmpty(dataKey))
            {
                return dataKey;
            }
            if (node.HasProperty(aliasName) && node.HasValue(aliasName))
            {
                var mediaId = node.GetPropertyValue<int>(aliasName);
                if (mediaId != 0)
                {
                    var newImage = _publishedContent.TypedMedia(mediaId);

                    SiteCacheHelper.AddToCache(cacheKey, newImage, new TimeSpan(1, 0, 0));
                    return newImage != null ? newImage.Url : "";
                }
            }
            return "";
        }


        public IPublishedContent GetTypedContent(IPublishedContent node, string aliasName)
        {
            var cacheKey = node.Id + aliasName;
            var dataKey = SiteCacheHelper.GetFromCache(cacheKey) as IPublishedContent;
            if (dataKey != null)
            {
                return dataKey;
            }
            if (node.HasProperty(aliasName) && node.HasValue(aliasName))
            {
                var pathId = node.GetPropertyValue<int>(aliasName);
                if (pathId == 0) return null;
                var publishedContent = new UmbracoHelper(UmbracoContext.Current);
                var contentNode = publishedContent.TypedContent(pathId);
                SiteCacheHelper.AddToCache(cacheKey, contentNode, new TimeSpan(1, 0, 0));
                return contentNode;
            }
            return null;
        }
    }
}