﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AbleSystems.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace AbleSystems.Web.Controllers
{
    public class ContactFormController : SurfaceController
    {
        public static class EmailSettings
        {
            public static string CustomerMessage { get; set; }
            public static string AdminMessage { get; set; }
            public static string EmailTo { get; set; }
            public static string EmailFrom { get; set; }
            public static string SubjectLine { get; set; }
        }
        
        public ActionResult Index(){
       
            return PartialView("Partials/ContactForm");
        }

        [HttpPost, ValidateInput(true)]
        public ActionResult Submit(ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                if (TempData.ContainsKey("validationMessage"))
                    TempData.Remove("validationMessage");

                TempData.Add("validationMessage", "Please ensure you complete all the fields.");
                return RedirectToCurrentUmbracoPage();
            }

            if (TempData.ContainsKey("validationMessage"))
                TempData.Remove("validationMessage");
            HandleSubmit(model);
            MergeFields(model);

            if (model.ThankYouPageId != 0)
                return RedirectToUmbracoPage(model.ThankYouPageId);

            else
                return RedirectToCurrentUmbracoPage();
        }
    
        internal void GetEmailSettings(ContactFormModel model)
        {
            var umbracoHelper = SiteHelper.GetUmbracoHelper();
            var contactPage = umbracoHelper.TypedContent(CurrentPage.Id);
            var solutionName = GetSolutionName(model.Solution);

            EmailSettings.SubjectLine = "Thank you for your enquiry";
            EmailSettings.CustomerMessage =
                "Thank you for for your enquiry, it has been passed to a member of our team and we will be in touch.";
            EmailSettings.AdminMessage =
                string.Format(
                    "A new contact form enquiry has been completed, details are below:" + Environment.NewLine + Environment.NewLine +
                    "Name: {0}" +Environment.NewLine  +
                    "Email: {1}" + Environment.NewLine +
                    "Phone Number: {2}" + Environment.NewLine +
                    "Company: {3}" +Environment.NewLine +
                    "Type of solution: {4}" + Environment.NewLine +
                    "Comments: {5}" + Environment.NewLine +
                    "Tick here if you do not wish to be added to our mailing list: {6}",
                    model.FirstName + " " + model.LastName, model.Email, model.Phone, model.CompanyName, solutionName, model.Message, model.MailingList);

            EmailSettings.EmailTo = EmailTo(solutionName);
            EmailSettings.EmailFrom = SiteHelper.GetSiteGlobalProperty("notificationEmail");

            /* Check for customised messages from Umbraco       */

            if (contactPage.HasProperty("subjectLine") && contactPage.HasValue("subjectLine"))
                EmailSettings.SubjectLine = contactPage.GetPropertyValue<string>("subjectLine");

            if (contactPage.HasProperty("customerMessage") && contactPage.HasValue("customerMessage"))
                EmailSettings.CustomerMessage = contactPage.GetPropertyValue<string>("customerMessage");

            if (contactPage.HasProperty("adminMessage") && contactPage.HasValue("adminMessage"))
                EmailSettings.AdminMessage = contactPage.GetPropertyValue<string>("adminMessage");
        }


        public string GetSolutionName(string solution)
        {
            var umbracoHelper = SiteHelper.GetUmbracoHelper();

            if (umbracoHelper.TypedContent(solution) != null)
            {
                return umbracoHelper.TypedContent(solution).Name;
            }
            else
            {
                if (solution != null)
                {
                    return solution;
                }
                else
                {
                    return "";
                }
            }
        }

        public string EmailTo(string solutionName)
        {
            if (solutionName == "Place an order" || solutionName == "Request a quotation" || solutionName == "Request a call back")
            {
                return SiteHelper.GetSiteGlobalProperty("esalesEmailAddress");
            }
            else if (solutionName == "Technical support")
            {
                return SiteHelper.GetSiteGlobalProperty("supportEmailAddress");
            }
            else
            {
                return SiteHelper.GetSiteGlobalProperty("siteEmailAddress");
            }
        }

        public void MergeFields(ContactFormModel model)
        {
            EmailSettings.CustomerMessage = EmailSettings.CustomerMessage.Replace("[name]", model.FirstName + " " + model.LastName).Replace("[phone]", model.Phone).Replace("[email]", model.Email).Replace("[comments]", model.Message);
            EmailSettings.AdminMessage = EmailSettings.AdminMessage.Replace("[name]", model.FirstName + " " + model.LastName).Replace("[phone]", model.Phone).Replace("[email]", model.Email).Replace("[comments]", model.Message);
        }

        internal void HandleSubmit(ContactFormModel model)
        {
            GetEmailSettings(model);

            var smtpClient = new SmtpClient
            {
                Port = 25,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = "mars.obsidianmail.com",
                Credentials = new NetworkCredential("mail@able-systems.com", "uhwykgax")
            };

            /*  Customer email     */

            using (var mail = new MailMessage(EmailSettings.EmailFrom, model.Email))
            {
                mail.Subject = "A new email enquiry has been submitted";
                mail.Body = EmailSettings.CustomerMessage;
                smtpClient.Send(mail);
            }

            /*  Admin email     */

            using (var mail = new MailMessage(EmailSettings.EmailFrom, EmailSettings.EmailTo))
            {
                mail.Subject = EmailSettings.SubjectLine;
                mail.Body = EmailSettings.AdminMessage;
                smtpClient.Send(mail);
            }
        }
    }
}
