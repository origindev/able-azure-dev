﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbleSystems.Web.Models;
using Lucene.Net.Search;
using Examine;
using Examine.SearchCriteria;
using Examine.LuceneEngine.SearchCriteria;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using AbleSystems.Web.Controllers;

namespace AbleSystems.Web.Controllers
{
    public class SearchController : SurfaceController
    {
        private List<string> componentDocTypeList = new List<string> { };
        private List<string> settingsDocTypeList = new List<string> { };

        public SearchController()
        {
            // initialise component doctype list here
            // try to dynamically get list of doctypes that are components

            if (componentDocTypeList == null || !componentDocTypeList.Any())            
                componentDocTypeList = GetComponentDocumentTypes();
            

            if (settingsDocTypeList == null || !settingsDocTypeList.Any())            
                settingsDocTypeList = GetSettingsDocumentTypes();            
        }

        public List<string> GetComponentDocumentTypes()
        {
            var componentDocTypes = new List<string>();

            var settingsNodes = SiteHelper.GetSiteSettings();
            if (settingsNodes != null)
            {
                var builderLanding = settingsNodes.Descendants().Where(x => x.DocumentTypeAlias.Equals("componentBuilder"));

                if (builderLanding != null && builderLanding.Any())

                    foreach (var component in builderLanding.FirstOrDefault().Descendants())

                        if (!componentDocTypes.Contains(component.DocumentTypeAlias))
                            componentDocTypes.Add(component.DocumentTypeAlias);

            }

            return componentDocTypes;
        }

        public List<string> GetSettingsDocumentTypes()
        {
            var settings = new List<string>();

            var settingsNodes = SiteHelper.GetSiteSettings();
            if (settingsNodes != null)
            {
                var settingsChildNodes = settingsNodes.DescendantsOrSelf();

                if (settingsChildNodes != null && settingsChildNodes.Any())

                    foreach (var component in settingsChildNodes.FirstOrDefault().Children)

                        if (!settingsDocTypeList.Contains(component.DocumentTypeAlias))
                            settingsDocTypeList.Add(component.DocumentTypeAlias);


            }

            return settingsDocTypeList;
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult Search(SearchModel model)
        {
            var searchLanding = GetSearchLanding();
            var searchResults = DoSearch(model, System.Web.HttpContext.Current);

            // Reorder to ensure products near top:
            searchResults = EnsureProductWeighting(searchResults);

            // If user has a previous search stored, clear the data...
            if (TempData.ContainsKey("searchResults"))
                TempData.Remove("searchResults");

            // Store results 
            TempData.Add("searchresults", searchResults);
            
            return RedirectToUmbracoPage(searchLanding.Id);
        }

        public SearchModel DoSearch(SearchModel searchModel, HttpContext context)
        {
            var searchTerm = searchModel.Text ?? "";
            var searchResults = ExamineManager.Instance.Search(searchTerm, true);
            var componentIdsToSearch = new List<int>(); // if any search results hit components, we should not return them, but their parent (discarding any duplicates)

            foreach (var result in searchResults)
            {
                if (result != null)
                {
                    var resultNode = Umbraco.TypedContent(result.Id);
                    if (resultNode != null)
                    {
                        // if this is a component, we then need to search for any item that includes this component 

                        if (IsAComponent(resultNode))
                        {
                            if (!componentIdsToSearch.Contains(resultNode.Id))
                                componentIdsToSearch.Add(resultNode.Id);
                        }
                        else if (!IsASiteSetting(resultNode))
                        {
                            // otherwise, simply add to search results list as a regular result node
                            // (provided it's not a site setting node!)

                            searchModel.results.Add(resultNode);
                        }
                    }
                }
            }       

            // if we have any component id's, we must do a separate search for these, and then combine into main search results (discarding duplicates)

            if (componentIdsToSearch.Any())
            {
                var componentSearcher = ExamineManager.Instance.DefaultSearchProvider;

                foreach (var componentId in componentIdsToSearch)
                {
                    var internalSearchCollection = ExamineManager.Instance.SearchProviderCollection["InternalMemberSearcher"];
                    var searchComponentCriteria = internalSearchCollection.CreateSearchCriteria(BooleanOperation.Or);
                    var luceneString = "+componentPicker:*" + componentId + "* ";
                    var searchQuery = searchComponentCriteria.RawQuery(luceneString);

                    var nodesThatUseComponent = componentSearcher.Search(searchQuery);

                    foreach (var nodeThatUsesComponent in nodesThatUseComponent)
                    {
                        var nodeUsingComponent = Umbraco.TypedContent(nodeThatUsesComponent.Id);

                        if (nodeUsingComponent != null && !searchModel.results.Contains(nodeUsingComponent))
                        {
                            searchModel.results.Add(nodeUsingComponent);
                        }
                    }
                }
            }
                
            searchModel.TotalResults = searchModel.results.Count();
            searchModel.TotalPages = (int)Math.Ceiling((double)searchModel.TotalResults / searchModel.ResultsPerPage);

            return searchModel;
        }

        public SearchModel EnsureProductWeighting(SearchModel resultList)
        {
            // split by those that are a product and those that are not
            var products = new List<IPublishedContent>();

            foreach (var result in resultList.results)
            {
                if (result.DocumentTypeAlias.Equals("productRange"))
                {
                    products.Add(result);
                }
            }

            if (products != null && products.Any())
            {
                resultList.results.RemoveAll(x => x.DocumentTypeAlias.Equals("productRange"));
                resultList.results = products.Union(resultList.results).ToList();
            }

            return resultList;
        }

        public IPublishedContent GetSearchLanding()
        {
            var homes = SiteHelper.GetSiteRoots().Where(x => x.DocumentTypeAlias.Equals("homepage"));

            if (homes != null)
            {
                var search = homes.FirstOrDefault().Children.Where(x => x.DocumentTypeAlias.Equals("searchLanding"));

                if (search != null && search.Any())
                    return search.FirstOrDefault();
            }

            return homes.FirstOrDefault();
        }

        public static IPublishedContent GetSearchLanding(List<IPublishedContent> rootNode)
        {
            var home = rootNode.FirstOrDefault(x => x.ContentType.Alias.Equals("homepage"));

            if (home != null)
            {
                var search = home.Children.Where(x => x.DocumentTypeAlias.Equals("searchLanding"));

                if (search != null && search.Any())
                    return search.FirstOrDefault();
            }

            return home;
        }

        public static string GetResultBody(IPublishedContent result)
        {
            var resultBody = string.Empty;

            if (result != null)
            {
                if (result.HasProperty("metaDescription") && result.HasValue("metaDescription"))
                    resultBody = result.GetPropertyValue<string>("metaDescription");

                else if (result.HasProperty("bodyText") && result.HasValue("bodyText"))
                {
                    resultBody = result.GetPropertyValue<string>("bodyText");
                    resultBody = StripHtmlTags(resultBody);
                }

                if (resultBody != null && resultBody.Length > 200)
                {
                    resultBody = resultBody.Substring(0, 200) + " ... ";
                }

            }

            return resultBody;
        }

        public static string GetResultTitle(IPublishedContent result)
        {
            if (result != null)
            {
                if (result.HasProperty("articleTitle") && result.HasValue("articleTitle"))
                    return result.GetPropertyValue<string>("articleTitle");

                return result.Name;
            }

            return string.Empty;
        }

        public static string StripHtmlTags(string html)
        {
            if (String.IsNullOrEmpty(html)) return "";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            return HttpUtility.HtmlDecode(doc.DocumentNode.InnerText);
        }

        public bool IsAComponent(IPublishedContent node)
        {
            // need to rebuild list if not already populated ...
            if (componentDocTypeList == null || !componentDocTypeList.Any())
            {
                componentDocTypeList = GetComponentDocumentTypes();
            }

            return componentDocTypeList.Contains(node.DocumentTypeAlias);
        }

        public bool IsASiteSetting(IPublishedContent node)
          {
            // need to rebuild list if not already populated ...
              if (settingsDocTypeList == null || !settingsDocTypeList.Any())
            {
                settingsDocTypeList = GetSettingsDocumentTypes();
            }

            return settingsDocTypeList.Contains(node.DocumentTypeAlias);
        }
    }
}