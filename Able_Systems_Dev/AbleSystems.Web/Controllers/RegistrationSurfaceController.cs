﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AbleSystems.Web.Models;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace AbleSystems.Web.Controllers
{
    public class RegistrationSurfaceController : SurfaceController
    {
        private static readonly string SiteEmailAddress = SiteHelper.GetSiteGlobalProperty("siteEmailAddress");
        private static readonly string NoReplyEmailAddress = SiteHelper.GetSiteGlobalProperty("notificationEmail");

        public static class EmailSettings
        {
            public static string CustomerMessage { get; set; }
            public static string AdminMessage { get; set; }
            public static string EmailTo { get; set; }
            public static string EmailFrom { get; set; }
            public static string SubjectLineCustomer { get; set; }
            public static string SubjectLineAdmin { get; set; }
        }


        /// <summary>
        /// This is the main registration point for users on the site
        /// </summary>
        /// <param name="registerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RegisterSubmit(LoginRegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                TempData["registrationError"] =
                    "Please check you have completed all fields";
                return CurrentUmbracoPage();
            }

            try
            {
                GetEmailSettings(registerModel);
                DoMerge(registerModel);

                // Create the message
                var customerEmail = new MailMessage(EmailSettings.EmailFrom, EmailSettings.EmailTo)
                {
                    Subject = "A new user has registered",
                    SubjectEncoding = Encoding.UTF8,
                    ReplyToList = {EmailSettings.EmailFrom},
                    Body = EmailSettings.AdminMessage,
                    IsBodyHtml = true
                };

                // Create the message
                var adminEmail = new MailMessage(EmailSettings.EmailFrom, registerModel.Email)
                {
                    Subject = "A new user has registered",
                    SubjectEncoding = Encoding.UTF8,
                    ReplyToList = {EmailSettings.EmailFrom},
                    Body = EmailSettings.CustomerMessage,
                    IsBodyHtml = true
                };


                var smtpClient = new SmtpClient
                {
                    Port = 25,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "mars.obsidianmail.com",
                    Credentials = new NetworkCredential("mail@able-systems.com", "uhwykgax")
                };
                smtpClient.Send(adminEmail);
                smtpClient.Send(customerEmail);
                return RedirectToUmbracoPage(1306);
            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, "Error sending registration email", ex);
            }

            TempData["registrationError"] =
                "Sorry but there was an error processing your registration. Please contact <a href=\"mailto:" +
                SiteEmailAddress + "\">Able Systems</a>";

            if(registerModel.productUpdates==true)
            {
                var news = new NewsletterModel() { Email = registerModel.Email };
                RegisterForNewsletter(news);
            }

            return CurrentUmbracoPage();
        }

        [HttpPost, ValidateInput(true)]
        public ActionResult RegisterForNewsletter(NewsletterModel model)
        {
            if (!ModelState.IsValid)
            {
                if (TempData.ContainsKey("newsletterValidationMessage"))
                    TempData.Remove("newsletterValidationMessage");

                TempData.Add("newsletterValidationMessage", "Please enter your email address.");
                return RedirectToCurrentUmbracoPage();
            }

            RegisterSubscriberForNewsletter(model.Email);

            return RedirectToCurrentUmbracoPage();
        }

        internal void RegisterSubscriberForNewsletter(string emailAddress)
        {
            GetEmailSettings(emailAddress, SiteHelper.GetSiteGlobalProperty("customerRegistrationEmail"));

            // Create the message
            var adminEmail = new MailMessage(EmailSettings.EmailFrom, EmailSettings.EmailFrom)
            {
                Subject = "A new user has registered for the newsletter",
                SubjectEncoding = Encoding.UTF8,
                ReplyToList = { EmailSettings.EmailFrom },
                Body = EmailSettings.AdminMessage,
                IsBodyHtml = true
            };

            var customerEmail = new MailMessage(EmailSettings.EmailFrom, emailAddress)
            {
                Subject = EmailSettings.SubjectLineCustomer,
                SubjectEncoding = Encoding.UTF8,
                ReplyToList = { EmailSettings.EmailFrom },
                Body = EmailSettings.CustomerMessage,
                IsBodyHtml = true
            };

            var smtpClient = new SmtpClient();
            smtpClient.Send(adminEmail);
            smtpClient.Send(customerEmail);
        }


        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginRegisterModel model)
        {
            if (!string.IsNullOrEmpty(model.LoginPass))
            {
                var password = SiteHelper.GetSiteGlobalProperty("siteAccessPassword");

                if (!string.IsNullOrWhiteSpace(password))
                {
                    if (model.LoginPass.Equals(password))
                    {
                        // login is successful, set cookie and redirect

                        SetCookie("ableAuthCookie", "1");

                        var home = SiteHelper.GetSiteRoots();
                        if (home != null)
                        {
                            var supportAndDownloads =
                                home.FirstOrDefault()
                                    .Children.Where(x => x.DocumentTypeAlias.Equals("supportAndDownload"));

                            if (supportAndDownloads != null)
                            {
                                return RedirectToUmbracoPage(supportAndDownloads.FirstOrDefault().Id);
                            }
                            else CurrentUmbracoPage();
                        }
                        else return CurrentUmbracoPage();
                    }
                    else
                    {
                        // login fail,

                        TempData["loginError"] = "Password was incorrect, please check and try again.";
                        return CurrentUmbracoPage();
                    }
                }
                return CurrentUmbracoPage();
            }

            TempData["loginError"] = "Please enter a password to login";
            return CurrentUmbracoPage();
        }

        private void SetCookie(string name, string value)
        {
            HttpCookie myCookie = new HttpCookie(name);
            myCookie.Expires = DateTime.Now.AddYears(1);
            myCookie.Value = value;
            Response.Cookies.Add(myCookie);
        }

        internal void GetEmailSettings(LoginRegisterModel model)
        {
            var umbracoHelper = SiteHelper.GetUmbracoHelper();

            // defaults

            EmailSettings.SubjectLineCustomer = "Thank you for your enquiry";
            EmailSettings.SubjectLineAdmin = "A new user has registered on the website";
            EmailSettings.CustomerMessage =
                "Thank you for registering with Able Systems, you can access the site  your enquiry, it has been passed to a member of our team and we will be in touch.";

            EmailSettings.AdminMessage =
                string.Format(
                    "A new user has registered on the website, details are below:" + Environment.NewLine +
                    Environment.NewLine + "Name:{0} " + Environment.NewLine + "Email{1}" + Environment.NewLine +
                    "Phone:{2}" + Environment.NewLine + "Company:{3}" + Environment.NewLine + "Product Updates:{4}",
                    model.Title + " " + model.FirstName + " "+model.LastName, model.Email, model.Phone, model.CompanyName, model.productUpdates);


            // thank you email to customer with password:

            var emailToCustomer = umbracoHelper.TypedContent(SiteHelper.GetSiteGlobalProperty("downloadPasswordTemplate"));
            if (emailToCustomer != null)
            {
                EmailSettings.CustomerMessage = emailToCustomer.GetPropertyValue<string>("bodyText");
                EmailSettings.SubjectLineCustomer = emailToCustomer.GetPropertyValue<string>("subject");
            }

            var emailToAdmin= umbracoHelper.TypedContent(SiteHelper.GetSiteGlobalProperty("registrationTemplateSelector"));

            if (emailToAdmin != null)
            {
                EmailSettings.AdminMessage = emailToAdmin.GetPropertyValue<string>("bodyText");
                EmailSettings.SubjectLineAdmin = emailToAdmin.GetPropertyValue<string>("bodyText");
            }

            EmailSettings.EmailTo = SiteHelper.GetSiteGlobalProperty("siteEmailAddress");
            EmailSettings.EmailFrom = SiteHelper.GetSiteGlobalProperty("notificationEmail");
        }
        internal void GetEmailSettings(string emailToSingle, string NodeId)
        {
            var umbracoHelper = SiteHelper.GetUmbracoHelper();
            var emailTemplate = Umbraco.TypedContent(NodeId);

            if (emailTemplate != null)
            {
                // defaults

                EmailSettings.SubjectLineCustomer = "Thank you for your enquiry";
                EmailSettings.SubjectLineAdmin = "A new user has registered for the newsletter";
                EmailSettings.CustomerMessage =
                    emailTemplate.GetPropertyValue<string>("bodyText");

                EmailSettings.AdminMessage =
                    string.Format(
                        "A new user {0} has registered for the Able Systems newsletter" + Environment.NewLine, emailToSingle);

                EmailSettings.EmailTo = SiteHelper.GetSiteGlobalProperty("siteEmailAddress");
                EmailSettings.EmailFrom = SiteHelper.GetSiteGlobalProperty("notificationEmail");
            }
        }


        public void DoMerge(LoginRegisterModel model)
        {
            EmailSettings.AdminMessage =
                EmailSettings.AdminMessage.Replace("[firstname]", model.FirstName)
                .Replace("[companyname]", model.CompanyName)
                .Replace("[title]", model.Title)
                .Replace("[phone]", model.Phone)
                .Replace("[email]", model.Email)
                .Replace("[lastname]", model.LastName)
                .Replace("[productUpdates]", model.productUpdates.ToString());

            EmailSettings.CustomerMessage = EmailSettings.CustomerMessage.Replace("[password]",
                SiteHelper.GetSiteGlobalProperty("siteAccessPassword")).Replace("[firstname]", model.FirstName).Replace("[lastname]", model.LastName);
        }
    }
}