﻿using System;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AbleSystems.Web.Controllers
{
 
    /// <summary>
    ///     Random string generators.
    /// </summary>
    internal static class RandomUtil
    {
        /// <summary>
        ///     Get random string of 11 characters.
        /// </summary>
        /// <returns>Random string.</returns>
        public static string GetRandomString()
        {
            var path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path;
        }
    }

    public class Crypto
    {
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        /// <summary>
        ///     Encrypt the given string using AES.  The string can be decrypted using
        ///     DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static string EncryptStringAes(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr; // Encrypted string to return
            RijndaelManaged aesAlg = null; // RijndaelManaged object used to encrypt the data.

            try
            {
                // generate the key from the shared secret and the salt
                var key = new Rfc2898DeriveBytes(sharedSecret, Salt);

                // Create a RijndaelManaged object
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                // Create a decrytor to perform the stream transform.
                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                    }
                    //Write all data to the stream.

                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return outStr;
        }

        /// <summary>
        ///     Decrypt the given string.  Assumes the string was encrypted using
        ///     EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static string DecryptStringAes(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext;

            var decyrpt = cipherText.Split('?');

            try
            {
                // generate the key from the shared secret and the salt
                var key = new Rfc2898DeriveBytes(sharedSecret, Salt);

                // Create the streams used for decryption.
                var bytes = Convert.FromBase64String(decyrpt[0]);
                using (var msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);

                    // Create a decrytor to perform the stream transform.
                    var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }

        private static byte[] ReadByteArray(Stream s)
        {
            var rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            var buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }

    public static class Secure
    {
        // The secret key known by both parties
        private const string SecretKey = "ablesystemssecurekeystring123";

        public static string CreateAuthString(IMember m)
        {
            return "?token=" + Crypto.EncryptStringAes(m.Id + "-" + GenerateSignatureString(), SecretKey);
        }

        private static string GenerateSignatureString()
        {
            // Create the MD5 hasher and a UTF8Encoding class
            var hasher = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();

            // Create the hash
            var contentToSignData = encoder.GetBytes(SecretKey);
            var signatureData = hasher.ComputeHash(contentToSignData);
            var signatureString = Convert.ToBase64String(signatureData);
            return signatureString;
        }

        public static bool AuthenticateMember(string authString)
        {
            if (string.IsNullOrEmpty(authString))
            {
                HttpContext.Current.Response.Redirect("/");
            }
            else
            {
                var splitAuth = Crypto.DecryptStringAes(authString.Replace(" ", "+"), SecretKey).Split('-');
                // first element = garbage, 2nd element is userid, 3rd element is the token itself

                // Compute the signature ourselves and compare it to the submitted signature
                var hasher = new MD5CryptoServiceProvider();
                var encoder = new UTF8Encoding();

                // Create the hash
                var contentToSignData = encoder.GetBytes(SecretKey);
                var signatureData = hasher.ComputeHash(contentToSignData);
                var signatureString = Convert.ToBase64String(signatureData);

                var userId = Convert.ToInt32(splitAuth[0]);

                if (String.CompareOrdinal(splitAuth[1], signatureString) != 0)
                {
                    // "Identity token has been tampered!";
                    return false;
                }
                
                // "Identity token is authentic!!";
                var membershipService = ApplicationContext.Current.Services.MemberService;
                var authenticatedMember = membershipService.GetById(userId);
                if (authenticatedMember != null)
                {
                    authenticatedMember.IsApproved = true;
                    membershipService.Save(authenticatedMember);

                    var siteAccessPassword = SiteHelper.GetSiteGlobalProperty("siteAccessPassword");
                     var noReplyEmailAddress = SiteHelper.GetSiteGlobalProperty("notificationEmail");

                    // now send them an email
                    var client = new SmtpClient();
                    var fromAddress = new MailAddress(noReplyEmailAddress, "Able Systems");
                    var to = new MailAddress(authenticatedMember.Email.Trim(), authenticatedMember.Name.Trim());

                    var templateId = SiteHelper.GetSiteGlobalProperty("downloadPasswordTemplate"); // mandatory so should never be null
                    var publishedContent = new UmbracoHelper(UmbracoContext.Current);
                    var registerMember = publishedContent.TypedContent(templateId);


                    var registerMemberMailBody = registerMember.GetPropertyValue<string>("bodyText").
                        Replace("{name}", authenticatedMember.Name.Trim()).
                        Replace("{password}", siteAccessPassword);

                    // Create the message
                    var msg = new MailMessage(fromAddress, to)
                    {
                        Subject = registerMember.GetPropertyValue<string>("subject"),
                        SubjectEncoding = Encoding.UTF8,
                        ReplyToList = { fromAddress },
                        Body = registerMemberMailBody,
                        IsBodyHtml = true
                    };

                    client.Send(msg); // Send the email

                    return true;
                }
            }

            return false;
        }
    }
}
