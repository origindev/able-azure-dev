﻿#region info

// 
//  Copyright (C) 2016 Simon Antony Ltd www.simonantony.net - All Rights Reserved
//  You may use, distribute and modify this code under the
//  terms of the supplied license to the company that commissioned
//  this work, any other copying or usage by unauthorised 3rd parties is forbidden
// 
// Written by: Simon Steed
// Client: AbleSystems
// Project Name: AbleSystems.Web
// Date: 04/10/2016

#endregion

#region Using Clauses

using System;
using System.Text;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.WebApi;

#endregion

namespace AbleSystems.Web.Controllers
{
    public class ProductSelectionController : UmbracoApiController
    {
        public string FindProduct(int id)
        {
            if (id == 0)
                return "0";
            var umb = new UmbracoBuilder();
            var mod = Umbraco.TypedContent(id);
            foreach (var item in mod.Children)
            {
                /*  If we have reached the end, then there will likely only be one child - the product selection, 
                 * if so, render product range, rather than the questions       */

                if (item.DocumentTypeAlias.Equals("selectionFinalAnswer"))
                {
                    if (item.HasProperty("productSelection") && item.HasValue("productSelection"))
                    {
                        var sb = new StringBuilder();

                        var resultsHaveTitle = item.HasProperty("articleTitle") && item.HasValue("articleTitle");
                        var resultsHasDescriptionText = item.HasProperty("bodyText") && item.HasValue("bodyText");

                        if ((resultsHaveTitle) || (resultsHasDescriptionText))
                        {
                            sb.Append("<div class=\"container\">");
                            sb.Append("<div class=\"row\"><div class=\"col-xs-12\"><header class=\"wrapper-header\">");
                            if (resultsHaveTitle)
                            {
                                sb.Append("<h2>" + item.GetPropertyValue("articleTitle") + "</h2>");
                            }
                            if (resultsHasDescriptionText)
                            {
                                sb.Append("<p>" + item.GetPropertyValue("bodyText") + "</p>");
                            }
                            sb.Append("</header></div></div>"); //header col row
                        }

                        sb.Append("<div class=\"row product-range-carousel\">");

                        foreach (var productItem in item.GetPropertyValue<string>("productSelection").Split(','))
                        {
                            var product = Umbraco.TypedContent(productItem);

                            if (product != null)
                            {
                                var productName = product.Name;
                                var productImage = string.Empty;
                                var productLink = "#";
                                var productLinkText = "View";

                                if (product.HasValue("productTitle"))
                                {
                                    productName = product.GetPropertyValue<string>("productTitle");
                                }

                                if (product.HasValue("productImage"))
                                {
                                    var productImageNode =
                                        Umbraco.TypedMedia(product.GetPropertyValue<string>("productImage"));
                                    if (productImageNode != null)
                                    {
                                        productImage = productImageNode.Url;
                                    }
                                }

                                if (product.HasValue("productLink"))
                                {
                                    var productLinkNode =
                                        Umbraco.TypedContent(product.GetPropertyValue<string>("productLink"));
                                    if (productLinkNode != null)
                                    {
                                        productLink = productLinkNode.Url;
                                    }
                                }

                                if (product.HasValue("productLinkText"))
                                {
                                    var productLinkNode =
                                        Umbraco.TypedContent(product.GetPropertyValue<string>("productLinkText"));

                                    productLinkText = product.GetPropertyValue<string>("productLinkText");

                                }

                                sb.Append("<div class=\"col-xs-12 col-sm-6 col-md-4 pod-wrap\">");
                                sb.Append("<div class=\"pod\">");
                                sb.Append("<div class=\"img\" style=\"background-image: url(" + productImage + ")\">");
                                sb.Append("<a href=\"" + productLink + "\"></a></div><header><h3>" + productName + "</h3></header>");
                                sb.Append(product.GetPropertyValue("productDescription"));
                                sb.Append("<a href=\"" + product.Url + "\" class=\"custom-btn outlined red large pill\">" + productLinkText + "</a>");
                                sb.Append("</div></div>");

                            }
                        }

                        sb.Append("</div></div>");

                        return "2" + sb;
                    }
                }
                else
                {
                    var icon = umb.GetImageMediaId(item, "icon");
                    var startingPoint = 3;


                    var level = Math.Ceiling((double)(item.Level - startingPoint) / 2);
                    var sb = new StringBuilder();
                    sb.Append("<div class=\"col-xs-12 \"><div class=\"question-data\">");
                    sb.Append("<div class=\"qu-wrapper\"><div class=\"qu-num\">Question <span class=\"no\">" + level +
                              "</span> of 4</div><div class=\"qu\">" + item.Name + "</div></div>");
                    sb.Append("<ul class=\"answers-list\">");

                    foreach (var subItem in item.Children)
                    {
                        icon = umb.GetImageMediaId(subItem, "icon");
                        sb.Append("<li rel=\"" + subItem.Id + "\">");
                        sb.Append("<div class=\"answer\"  rel=\"" + subItem.Id + "\">");
                        sb.Append("<img src=\"" + icon.GetCropUrl(74, 60) + "\" alt=\"" + subItem.Name + "\" />");
                        sb.Append("</div>" + subItem.Name);
                        sb.Append("</li>");
                    }

                    sb.Append("</ul></div>");
                    sb.Append(
                        "<a href=\"\" class=\"custom-btn outlined red pill large next-question\" rel=\"\">Next</a></div>");

                    if (level == 2)
                    {
                        sb.Append(
                            "<ul class=\"list-unstyled question-progress-indicator list-inline\"><li class=\"completed\"></li><li class=\"completed\"></li><li class=\"\"></li><li class=\"\"></li></ul>");
                    }
                    else if (level == 3)
                    {
                        sb.Append(
                            "<ul class=\"list-unstyled question-progress-indicator list-inline\"><li class=\"completed\"></li><li class=\"completed\"></li><li class=\"completed\"></li><li class=\"\"></li></ul>");
                    }
                    else if (level == 4)
                    {
                        sb.Append(
                            "<ul class=\"list-unstyled question-progress-indicator list-inline\"><li class=\"completed\"></li><li class=\"completed\"></li><li class=\"completed\"></li><li class=\"completed\"></li></ul>");
                    }

                    return sb.ToString();
                }
            }

            return "0";
        }
    }
}
