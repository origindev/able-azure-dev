﻿function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width: e[a + 'Width'], height: e[a + 'Height'] };
}

var vpWidth = viewport().width; //matches mediaQuery
var vpHeight = viewport().height; //matches mediaQuery

$(window).resize(function () {
    vpWidth = viewport().width;
    vpHeight = viewport().height;
});
