/// <reference path="../libs/jquery.3.1.1.js" />
/// <reference path="../libs/jquery.sticky.js" />
/// <reference path="../libs/slick.js" />
/// <reference path="../libs/masonry.pkgd.js" />
/// <reference path="../libs/viewport.js" />
/// <reference path="../libs/jquery.selectBox.js" />

$(function () {

    var ableSystems = {};

    ableSystems.funcs = {
        clearfooter: function clearfooter() {
            var footerHeight = ableSystems.elements.footer.outerHeight();
            ableSystems.elements.mainContent.css('padding-bottom', footerHeight);
        },
        mobileNavDimBg: function () {
            ableSystems.elements.mobileNavMenu.dimBackground({
                darkness: 0.9
            });
        },
        disableNavScroll: function () {
            ableSystems.elements.mobileNavMenu.offcanvas({
                disableScrolling: false
            });
        },
        calculateLogInHeight: function () {

            if (vpWidth > 767 && ableSystems.elements.logInComponent.length > 0 && ableSystems.elements.logInComponent.find(ableSystems.elements.logInCols.length > 0)) {

                var height = ($(window).height()) - (ableSystems.elements.footer.outerHeight()) - (ableSystems.elements.navigation.height());
                $('.log-in-component').css('min-height', height);

            } else if (vpWidth < 768 && ableSystems.elements.logInComponent.length > 0 && ableSystems.elements.logInComponent.find(ableSystems.elements.logInCols.length > 0)) {
                if ($('.log-in-component')[0].hasAttribute("style")) {
                    $('.log-in-component').removeAttr("style")
                }
            }
        },
        initMap: function () {
            var ableSystemsLatLng = { lat: 53.265810, lng: -2.4889380 };

            var mapOptions = {
                
                zoom: 17,
                styles:
                    [
                        {
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#444444"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#b0b0b0"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }],
                center: ableSystemsLatLng,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                zoomcontrol:false

            };

            var map = new google.maps.Map(document.getElementById('locationMap'), mapOptions);
            

            var marker = new google.maps.Marker({
                position: ableSystemsLatLng,
                map: map
            });

            setPan(vpWidth);

            google.maps.event.addDomListener(window, "resize", function () {
                map.setCenter(ableSystemsLatLng);
                setPan(vpWidth);
            });

            function setPan(windowWidth) {

                if (windowWidth < 768) {
                    map.panBy(0, 200);
                } else if (windowWidth > 767 && windowWidth < 992) {
                    map.panBy(-200, 0);
                } else if (windowWidth > 992) {
                    map.panBy(-250, 0);
                }
            }
        },
        selectThisAnswer: function () {
            var $this = $(this);
            var activeClass = 'active';
            var relId = $this.attr('rel');

            $this.closest('ul').find('div.answer').removeClass(activeClass);
            $this.addClass(activeClass);

            $('.next-question').attr('rel', relId); // update id on target
        },
        getNextQuestionData: function (e) {
            e.preventDefault();
            var link = $(this);
            var pageId = link.attr("rel");
            //Call the SetFavourites url and update the link with the returned value
            $.post("/Umbraco/Api/ProductSelection/FindProduct/" + pageId, function (data) {
                if (data[0] === "2") {
                    var rootNode = $(".printer-app-component");
                    var productRangeCarousel = 'div.product-range-carousel';
                    var progressIndicator = 'ul.question-progress-indicator';

                    rootNode.removeClass("printer-app-component").addClass('product-range dark-theme');
                    rootNode.find(progressIndicator).remove();
                    rootNode.html(data.substring(1)); //add data from api call

                    if (rootNode.find(productRangeCarousel).length > 0) {
                        rootNode.find(productRangeCarousel).slick(ableSystems.settings.productRangeCarouselSettings);
                    }
                }
                else if (data === "0") {
                    alert('Error - no data');
                } else {
                    $('.data').html(data);
                }
            });
        }
    };

    ableSystems.elements = {
        footerCols: $('div.footer-col'),
        navigation: $('div#navigation'),
        mobileNavToggle: $('a.mobile-nav-toggle'),
        mobileNavMenu: $('#mobile-nav-menu'),
        footer: $('footer#main-footer'),
        mainContent: $('div#main-content'),
        matchHeightElm: $('.match-height'),
        searchResultsWrapper: $('div.search-results'),
        logInComponent: $('div.log-in-component'),
        logInCols: $('div.log-in-col'),
        circularCarousel: $('div.circular-carousel'),
        addOnsCarousel: $('div.addons-carousel'),
        blogListingWrapper: $('div.blog-grid'),
        masonryTextGrid: $('div.masonry-text-grid'),
        quoteCarousel: $('div.quote-carousel'),
        productRangeCarousel: $('div.product-range-carousel'),
        map: $('div#locationMap'),
        heroBannerCarousel: $('div.hero-banner'),
        promoCarousel: $('div.promo-carousel'),
        printerApp: $('div.printer-app-component'),
        moreInfoLink: $('a.more-info'),
        technicalDocumentation: $('div.technical-documentation'),
        videoLink: $('a.video-link'),
        shareBlogArticleButton: $('a.share-article-btn')
    };


    ableSystems.settings = {
        stickySettings: {
            topSpacing: 0
        },
        quoteCarouselSettings: {
            accessibility: true,
            adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false,
            dots: true
        },
        circularCarouselSettings: {
            accessibility: true,
            centerMode: true,
            slidesToShow: 1,
            centerPadding: '0px',
            arrows: true,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }
            ]
        },
        productRangeCarouselSettings: {
            accessibility: true,
            arrows: false,
            dots: true,
            adaptiveHeight: true,
            mobileFirst: true,
            slidesPerRow: 1,
            rows: 1,
            responsive: [
                {
                    breakpoint: 700,
                    settings: {
                        slidesPerRow: 2,
                        rows: 2
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesPerRow: 3,
                        rows: 5
                    }
                }
            ]
        },
        addOnsCarouselSettings: {
            arrows: false,
            mobileFirst: true,
            autoplay: false,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }
            ]
        },
        heroBannerCarouselSettings: {
            arrows: false,
            mobileFirst: true,
            autoplay: true,
            autoplaySpeed: 5000
        },
        promoCarouselSettings: {
            arrows: false,
            dots: true,
            adaptiveHeight: true
        },
        fancyBoxVideoSettings: {
            openEffect: 'none',
            closeEffect: 'none',
            autosize: false,
            helpers: {
                overlay: {
                    locked: false
                },
                media: {}
            }
        }
    };

    $(window).resize(function () {
        if (this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 150);
    });

    

    ableSystems.elements.shareBlogArticleButton.on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        $('ul.share-article-to-list').show();
    });

    ableSystems.elements.footerCols.matchHeight();

    ableSystems.elements.matchHeightElm.matchHeight();

    ableSystems.elements.navigation.sticky(ableSystems.settings.stickySettings);

    ableSystems.elements.mobileNavToggle.on('click', ableSystems.funcs.disableNavScroll);

    ableSystems.elements.mobileNavMenu.on('show.bs.offcanvas', ableSystems.funcs.mobileNavDimBg);

    ableSystems.elements.mobileNavMenu.on('hide.bs.offcanvas', function () { $.undim(); });

    ableSystems.funcs.clearfooter();

    ableSystems.funcs.calculateLogInHeight();

    //Quotation carousel
    if (ableSystems.elements.quoteCarousel.length > 0) {
        ableSystems.elements.quoteCarousel.each(function () {
            var thisCarousel = $(this);
            thisCarousel.slick(ableSystems.settings.quoteCarouselSettings);
        });
    };

    //circular Carousel
    if (ableSystems.elements.circularCarousel.length > 0) {
        ableSystems.elements.circularCarousel.each(function () {
            var thisCarousel = $(this);
            var carouselSlide = '.carousel-slide';
            var numberOfSlides = thisCarousel.find(carouselSlide).length;

            if (numberOfSlides === 1) {
                thisCarousel.find(carouselSlide).wrapAll('<div class="one-slide"></div>');
            } else if (numberOfSlides === 2) {
                thisCarousel.find(carouselSlide).wrapAll('<div class="two-slides"></div>');
            } else {
                thisCarousel.slick(ableSystems.settings.circularCarouselSettings);
            };

        });
    };

    //add ons carousel
    if (ableSystems.elements.addOnsCarousel.length > 0) {
        ableSystems.elements.addOnsCarousel.each(function () {
            var thisCarousel = $(this);

            thisCarousel.slick(ableSystems.settings.addOnsCarouselSettings);

        });
    };

    //product range carousel
    if (ableSystems.elements.productRangeCarousel.length > 0) {
        ableSystems.elements.productRangeCarousel.each(function () {
            var thisCarousel = $(this);

            thisCarousel.slick(ableSystems.settings.productRangeCarouselSettings);
        });
    };

    if (ableSystems.elements.promoCarousel.length > 0) {
        ableSystems.elements.promoCarousel.each(function () {
            var thisCarousel = $(this);

            thisCarousel.slick(ableSystems.settings.promoCarouselSettings);
        });
    };

    //hero banner carousel
    if (ableSystems.elements.heroBannerCarousel.length > 0) {
        ableSystems.elements.heroBannerCarousel.each(function () {
            var thisCarousel = $(this);

            thisCarousel.slick(ableSystems.settings.heroBannerCarouselSettings);
        });
    };

    //Search results - Masonry Grid
    if (ableSystems.elements.searchResultsWrapper.length > 0) {
        ableSystems.elements.searchResultsWrapper.masonry();
    };

    //Blog listing  - Masonry Grid
    if (ableSystems.elements.blogListingWrapper.length > 0) {
        ableSystems.elements.blogListingWrapper.masonry();
    };

    //masonry text
    if (ableSystems.elements.masonryTextGrid.length > 0) {
        ableSystems.elements.masonryTextGrid.masonry();
    }

    //technical results Masonry
    if (ableSystems.elements.technicalDocumentation.length > 0) {
        ableSystems.elements.technicalDocumentation.each(function () {
            $(this).masonry();
        });
    };

    ableSystems.elements.moreInfoLink.on('click', function (e) {
        e.preventDefault();

        var $thisLink = $(this);
        var $thisLinkTextContainer = $thisLink.find('span');
        var $thisLinkText = $thisLinkTextContainer.text();
        var $thisRangeItem = $thisLink.closest('li.range-item');
        var technicalDocumentationWrapper = $thisLink.closest('div.technical-documentation');
        var others = technicalDocumentationWrapper.find('li.range-item').not($thisRangeItem);

        $thisLinkTextContainer.text($thisLinkText === "More Info" ? "Less Info" : "More Info");
        $thisRangeItem.find('div.hidden-content').toggle();

        others.each(function () {
            var $this = $(this);

            $this.find('div.hidden-content').hide();
            $this.find('a.more-info span').text('More Info');
        });

        technicalDocumentationWrapper.masonry();

    });


    $('select.custom-select').selectBox({
        mobile: true
    });


    if (ableSystems.elements.map.length > 0) {
        ableSystems.funcs.initMap();
    }


    

    //printer app component
    if (ableSystems.elements.printerApp.length > 0) {

        var body = $('body');

        body.on("click", ".answer", ableSystems.funcs.selectThisAnswer);

        body.on("click", ".next-question", ableSystems.funcs.getNextQuestionData);
    };

    $(window).bind('resizeEnd', function () {
        ableSystems.funcs.clearfooter();
        ableSystems.elements.navigation.sticky('update');

        ableSystems.funcs.calculateLogInHeight();

        //** reinit all video links
        ableSystems.elements.videoLink.fancybox(ableSystems.settings.fancyBoxVideoSettings);

    });

    ableSystems.elements.videoLink.fancybox(ableSystems.settings.fancyBoxVideoSettings);

    $('ul.internal-navigation a').on('click', function (e) {
        var $thisHref = $(this).attr('href');
        var element = $($thisHref);
        var headerHeight = ableSystems.elements.navigation.height();
        var scrollTime = 2000;

        $('html, body').animate({
            scrollTop: element.offset().top - headerHeight
        }, scrollTime);

        return false;
    });

    $("a.back-to-top").click(function (e) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
});


//Hero Refresh
function refresh() {
    var bgImages = new Array(

            "/media/1553/shutterstock_196356746.jpg",

            "/media/1544/shutterstock_308426144.jpg",

            "/media/1543/shutterstock_265621241.jpg",

            "/media/1547/shutterstock_280947320.jpg",

            "/media/1006/smiling-chap.jpg"

        );


    element = document.getElementById("hero-refresh");

    if (element) {

        element.style.background = "linear-gradient(to left, rgba(0, 0, 0, 0.0), rgba(0, 0, 0, 0.55)), rgba(0, 0, 0, 0.0) url(" + bgImages[Math.floor(Math.random() * bgImages.length)] + ")";

    } else { }
}
jQuery(document).ready(refresh)

//FAQs
jQuery(document).ready(function () {
    $('.text-section-wrapper').css('position', 'relative');
    $('.text-section-wrapper').css('top', '0');
    $('.text-section-wrapper').css('left', '0');
});

$(".text-section h3").click(function () {
    $(this).parent().nextAll().slideToggle();
});

//Fixed Navigation
function fixDiv() {
    var $div = $("#tab-fixed");
    if ($(window).scrollTop() > $div.data("top")) {
        $div.addClass("tab-fixed");
    }
    else {
        $div.removeClass("tab-fixed");
    }
}

if ($("#tab-fixed").length > 0) {
    $("#tab-fixed").data("top", $("#tab-fixed").offset().top + (-134)); // set original position on load
}
$(window).scroll(fixDiv);


function fixDivTwo() {
    var $div = $("#tab-fixed-two");
    if ($(window).scrollTop() > $div.data("top")) {
        $div.addClass("tab-fixed");
    }
    else {
        $div.removeClass("tab-fixed");
    }
}

if ($("#tab-fixed-two").length > 0) {
    $("#tab-fixed-two").data("top", $("#tab-fixed-two").offset().top + (-134)); // set original position on load
}
$(window).scroll(fixDivTwo);
