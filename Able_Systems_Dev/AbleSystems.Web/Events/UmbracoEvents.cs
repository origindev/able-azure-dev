﻿using AbleSystems.Web.Controllers;
using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;
using ImageProcessor;
using ImageProcessor.Imaging;
using System.Reflection;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using File = System.IO.File;

namespace AbleSystems.Web.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        public UmbracoEvents()
        {
            MediaService.Saving += MediaService_Saving;
            ContentService.Published += ContentService_Published;
        }

        /*  Rebuild site cache on publish as nodes could be site settings   */
        private void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            SiteCacheHelper.ExpireAllCacheItems();
        }


          /// <summary>
        ///     This is called when media items are being saved. It loads the settings from the config appSettings.
        ///     Depending on the settings, it create a resized version of the images and optionally replace the original files.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void MediaService_Saving(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            var width = int.Parse(WebConfigurationManager.AppSettings["ImageResizeWidth"]);
            var height = int.Parse(WebConfigurationManager.AppSettings["ImageResizeHeight"]);
            var fileNameSuffix = WebConfigurationManager.AppSettings["ImageResizeSuffix"];
            var keepOriginal = bool.Parse(WebConfigurationManager.AppSettings["ImageResizeKeepOriginal"]);
            var upscale = bool.Parse(WebConfigurationManager.AppSettings["ImageResizeUpscale"]);

            foreach (var mediaItem in e.SavedEntities)
            {
                try
                {
                    if (mediaItem != null)
                    {
                        if (!string.IsNullOrEmpty(mediaItem.ContentType.Alias) &&
                            mediaItem.ContentType.Alias.ToLower() == "image")
                        {
                            var isNew = mediaItem.Id <= 0;
                            var currentWidth = (mediaItem.Properties["umbracoWidth"].Value != null
                                ? int.Parse(mediaItem.Properties["umbracoWidth"].Value.ToString())
                                : width);
                            var currentHeight = (mediaItem.Properties["umbracoHeight"].Value != null
                                ? int.Parse(mediaItem.Properties["umbracoHeight"].Value.ToString())
                                : height);
                            var isDesiredSize = (currentWidth == width) && (currentHeight == height);
                            var isLargeEnough = currentWidth >= width && currentHeight >= height;
                            if ((isNew && !isDesiredSize && (isLargeEnough || upscale))
                                && mediaItem.Properties["umbracoFile"].Value != null)
                            {
                                var filePath = (string) mediaItem.Properties["umbracoFile"].Value;
                                var originalFilePath = HttpContext.Current.Server.MapPath(filePath);
                                var newFilePath = GetNewFilePath(originalFilePath, fileNameSuffix);
                                if (CreateCroppedImage(originalFilePath, newFilePath, width, height))
                                {
                                    if (!keepOriginal)
                                    {
                                        if (DeleteFile(originalFilePath))
                                        {
                                            RenameFile(newFilePath, originalFilePath);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, "Error saving media file ", ex);
                }
            }
        }

        /// <summary>
        ///     Creates a cropped version of the image at the size specified in the parameters
        /// </summary>
        /// <param name="originalFilePath">The full path of the original file</param>
        /// <param name="newFilePath">The full path of the new file</param>
        /// <param name="width">The new image width</param>
        /// <param name="height">The new image height</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        private bool CreateCroppedImage(string originalFilePath, string newFilePath, int width, int height)
        {
            var success = false;
            try
            {
                var imageFactory = new ImageFactory();
                imageFactory.Load(originalFilePath);
                var layer = new ResizeLayer(new Size(width, height), ResizeMode.Crop, AnchorPosition.Center);
                imageFactory.Resize(layer);
                imageFactory.Save(newFilePath);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        ///     Creates a new file path using the original one and adding a suffix to the file name
        /// </summary>
        /// <param name="filePath">The full path of the original file</param>
        /// <param name="fileNameSuffix">The suffix to be used at the end of the file name in the new file path</param>
        /// <returns>The new file path</returns>
        public string GetNewFilePath(string filePath, string fileNameSuffix)
        {
            var fileInfo = new FileInfo(filePath);
            var folderPath = fileInfo.DirectoryName;
            var fileExtension = fileInfo.Extension;
            var fullFileName = fileInfo.Name;
            var fileNameWithoutExtension = fullFileName.Substring(0, fullFileName.Length - fileExtension.Length);
            return string.Format("{0}\\{1}{2}{3}", folderPath, fileNameWithoutExtension, fileNameSuffix, fileExtension);
        }

        /// <summary>
        ///     Deletes a file, if it exists
        /// </summary>
        /// <param name="filePath">The full path of the file to delete</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        public bool DeleteFile(string filePath)
        {
            var success = false;
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        ///     Renames a file by using the Move method.
        /// </summary>
        /// <param name="sourceFileName">The full path of the source file</param>
        /// <param name="destFileName">The full path of the destination file</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        public bool RenameFile(string sourceFileName, string destFileName)
        {
            var success = false;
            try
            {
                if (File.Exists(sourceFileName) && !File.Exists(destFileName))
                {
                    File.Move(sourceFileName, destFileName);
                    success = true;
                }
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }
    }    
}