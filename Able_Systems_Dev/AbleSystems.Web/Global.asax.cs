﻿#region info

// 
//  Copyright (C) 2016 Simon Antony Ltd www.simonantony.net - All Rights Reserved
//  You may use, distribute and modify this code under the
//  terms of the supplied license to the company that commissioned
//  this work, any other copying or usage by unauthorised 3rd parties is forbidden
// 
// Written by: Simon Steed
// Client: AbleSystems
// Project Name: AbleSystems.Web
// Date: 29/09/2016

#endregion

#region Using Clauses

using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

#endregion

namespace AbleSystems.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}