﻿#region info

// 
//  Copyright (C) 2016 Simon Antony Ltd www.simonantony.net - All Rights Reserved
//  You may use, distribute and modify this code under the
//  terms of the supplied license to the company that commissioned
//  this work, any other copying or usage by unauthorised 3rd parties is forbidden
// 
// Written by: Simon Steed
// Client: AbleSystems
// Project Name: AbleSystems.Web
// Date: 29/09/2016

#endregion

using System;
using System.Text.RegularExpressions;

namespace AbleSystems.Web
{
    public static class BooleanExtensions
    {
        public static string ToYesNoString(this bool? value)
        {
            if (value.HasValue && !value.Value) // MyBool is false
                return "No";
            if (value.HasValue) // MyBool is true
                return "Yes";
            return "No";
        }

        public static string ToYesNoString(this bool value)
        {
            return value ? "Yes" : "No";
        }
    }

    public static class IntegerExtensions
    {
        public static string ToOccurrenceSuffix(this int integer)
        {
            switch (integer % 100)
            {
                case 11:
                case 12:
                case 13:
                    return "th";
            }
            switch (integer % 10)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
    }   

    public static class DateTimeExtensions
    {
        public static string ToString(this DateTime dateTime, string format, bool useExtendedSpecifiers)
        {
            return dateTime.ToString(format)
                .Replace("nn", dateTime.Day.ToOccurrenceSuffix().ToLower())
                .Replace("NN", dateTime.Day.ToOccurrenceSuffix().ToUpper());
        }
    }

    /// <summary>
    ///     Summary description for StringExtensions
    /// </summary>
    public static class StringExtensions
    {
        public static string SanitiseForJs(this string input)
        {
            var inputString = "";

            if (input != null)
            {
                inputString = input;
            }
            return inputString.Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("'", "");
        }

        public static string StripHtml(this string s)
        {
            var str = Regex.Replace(s, "<(.|\\n)*?>", " ").Replace(Environment.NewLine, " ").Replace("\n", " ").Replace("  ", " ");
            return str;
        }

        public static string FormatDateTime(this string dateString, string format)
        {
            DateTime dateTime;
            var str = (DateTime.TryParse(dateString, out dateTime) ? dateTime.ToString(format) : string.Empty);
            var str1 = str;
            return str1;
        }

        public static string ReplaceQueryStringItem(this string url, string key, string value)
        {
            string str;
            var flag = url.Contains("?");
            if (flag)
            {
                flag = url.Contains(string.Concat(key, "="));
                str = (flag ? Regex.Replace(url, string.Concat("([?&]", key, ")=[^?&]+"), string.Concat("$1=", value)) : string.Format("{0}&{1}={2}", url, key, value));
            }
            else
            {
                str = string.Format("{0}?{1}={2}", url, key, value);
            }
            return str;
        }

        public static string NullSafeToString(this Object me)
        {
            return me == null ? String.Empty : me.ToString();
        }
    }
}