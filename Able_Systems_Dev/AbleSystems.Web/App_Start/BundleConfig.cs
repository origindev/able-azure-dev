﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Logging;

namespace AbleSystems.Web
{
    /// <summary>
    /// http://stackoverflow.com/questions/11979718/how-can-i-specify-an-explicit-scriptbundle-include-order
    /// </summary>
    internal class AsIsBundleOrderer : IBundleOrderer
    {
        public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }

    internal static class BundleExtensions
    {
        public static Bundle ForceOrdered(this Bundle sb)
        {
            sb.Orderer = new AsIsBundleOrderer();
            return sb;
        }
    }

    public class ApplicationEventHandler : IApplicationEventHandler
    {
        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
        }
    }

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        /// <summary>
        /// Bundle usage - in views call:
        /// @Scripts.Render("~/bundles/jquery", "~/bundles/jqueryval")
        /// @Styles.Render("~/bundles/style")
        /// </summary>
        /// <param name="bundles"></param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/js/libs/jquery.3.1.1.js").ForceOrdered());

            bundles.Add(new ScriptBundle("~/bundles/core").Include(
                "~/js/libs/jquery.selectBox.js",
                "~/js/libs/jquery.sticky.js",
                "~/js/libs/masonry.pkgd.js",
                "~/js/libs/jquery.dim-background.js",
                "~/js/libs/bootstrap.js",
                "~/js/libs/jasny-bootstrap.js",
                "~/js/libs/jquery.matchHeight.js",
                "~/js/libs/slick.js",
                "~/js/libs/viewport.js",
                "~/js/libs/jquery.cookie.js",
                "~/js/libs/jquery.cookiecuttr.js",
                "~/js/libs/jquery.fancybox.pack.js",
                "~/js/libs/jquery.fancybox-buttons.js",
                "~/js/libs/jquery.fancybox-media.js",
                "~/js/libs/animation/three.js",
                "~/js/libs/animation/Projector.js",
                "~/js/libs/animation/CanvasRenderer.js",
                "~/js/libs/animation/stats.min.js",
                "~/js/libs/animation/spinningDots.js"
                ).ForceOrdered());


            bundles.Add(new ScriptBundle("~/bundles/pages").Include(
                "~/js/pages/able-systems.js").ForceOrdered());

            //CSS
            bundles.Add(new StyleBundle("~/bundles/style").Include(
                "~/css/libs/bootstrap.css",
                "~/css/libs/jasny-bootstrap.css",
                "~/css/libs/bootstrap-extensions.css",
                "~/css/libs/slick.css",
                "~/css/libs/slick-theme.css",
                "~/css/libs/font-awesome.css",
                "~/css/libs/jquery.selectBox.css",
                "~/css/libs/cookiecuttr.css",
                "~/css/libs/jquery.fancybox.css",
                "~/css/libs/jquery.fancybox-buttons.css",
                "~/css/able-systems.css"
                ).ForceOrdered());


            LogHelper.Info<string>("Bundles Loaded");
        }
    }
}